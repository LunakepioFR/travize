import React, { useRef, useState, useEffect } from "react";
import gsap from "gsap";

export const Menu = ({ menuOpen, setMenuOpen }) => {
  function ListComponent({ name }) {
    const liRef = useRef(null);
    const overlayRef = useRef(null);
    const aRef = useRef(null);
    // //make overlay reveal text animation

    return (
      <span className="overlay" ref={overlayRef}>
        <a ref={liRef} href="">
          {name}
        </a>
      </span>
    );
  }

  const menuRef = useRef(null);
  // make listComponent appear one at a time
  const closeMenu = () => {
    const allA = menuRef.current.querySelectorAll(".overlay a");

    const menu = menuRef.current;
    gsap.to(menu, {
      y: "0%",
      duration: 0.5,
      ease: "power4.in",
    });
    allA.forEach((a, i) => {
      gsap.to(a, {
        y: "100%",
        duration: 0.2,
        ease: "expo.inOut",
        delay: i * 0.01,
      });
    });
    setTimeout(() => {
      setMenuOpen(false);
    }, 500);
  };

  useEffect(() => {
    const allA = menuRef.current.querySelectorAll(".overlay a");
    allA.forEach((a) => {
      gsap.set(a, { y: "100%" });
    });

    const menu = menuRef.current;
    if (menuOpen) {
      gsap.to(menu, {
        y: "100%",
        duration: 1,
        ease: "expo.inOut",
      });

      setTimeout(() => {
        allA.forEach((a, i) => {
          gsap.to(a, {
            y: "0%",
            duration: 0.3,
            ease: "expo.inOut",
            delay: i * 0.05,
          });
        });
      }, 500);
    }
  }, [menuOpen]);

  return (
    <div className="menuOverlay" ref={menuRef}>
      <div className="closeMenu" onClick={closeMenu}>
        <div className="line top"></div>
        <div className="line bottom"></div>
      </div>
      <ListComponent name="Accueil" />
      <ListComponent name="A propos" />
      <ListComponent name="Contact" />
      <ListComponent name="Connexion" />
    </div>
  );
};
