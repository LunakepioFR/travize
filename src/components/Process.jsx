import { useControls } from "leva";
import { useEffect, useRef } from "react";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

export const Process = () => {
  const stepOneRef = useRef();
  const stepTwoRef = useRef();
  const stepThreeRef = useRef();
  const stepFourRef = useRef();
  const stepFiveRef = useRef();
  const containerRef = useRef();

  useEffect(() => {
    if (containerRef.current) {
      const tl = gsap.timeline({
        scrollTrigger: {
          trigger: containerRef.current,
          start: "20% 80%",
          end: "60% 20%",
          scrub: 1,
          // markers: true,
          id: "process",
        },
      });

      gsap.set(stepOneRef.current, { y: 100, opacity: 0 });
      gsap.set(stepTwoRef.current, { y: 100, opacity: 0 });
      gsap.set(stepThreeRef.current, { y: 100, opacity: 0 });
      gsap.set(stepFourRef.current, { y: 100, opacity: 0 });
      gsap.set(stepFiveRef.current, { y: 100, opacity: 0 });

      tl.add("start").to(
        stepOneRef.current,
        { y: 0, opacity: 1, duration: 1 },
        "start"
      );
      tl.add("start2").to(
        stepTwoRef.current,
        { y: 0, opacity: 1, duration: 1 },
        "start2"
      );
      tl.add("start3").to(
        stepThreeRef.current,
        { y: 0, opacity: 1, duration: 1 },
        "start3"
      );
      tl.add("start4").to(
        stepFourRef.current,
        { y: 0, opacity: 1, duration: 1 },
        "start4"
      );
      tl.add("start5").to(
        stepFiveRef.current,
        { y: 0, opacity: 1, duration: 1 },
        "start5"
      );
    }
  }, []);
  return (
    <section ref={containerRef} className="process">
      <div className="stickyContainer">
        <h2>Comment ça marche ?</h2>
        <p>
          En seulement quelques étapes simples, vous ferez partie de notre
          communauté dynamique de voyageurs. Rejoignez-nous dès maintenant et
          découvrez de nouvelles aventures passionnantes avec des personnes
          partageant les mêmes intérêts que vous !
        </p>
        <div className="container">
          <div ref={stepOneRef} className="process-bloc step-1">
            <div className="badge">1</div>
            <h3>Inscription</h3>
            <p>
              Inscrivez-vous avec votre adresse e-mail pour profiter de tous nos
              avantages
            </p>
          </div>
          <div ref={stepTwoRef} className="process-bloc step-2">
            <div className="badge">2</div>
            <h3>Remplir le formulaire</h3>
            <p>
              Remplissez le formulaire dès maintenant et laissez-nous prendre
              soin de tous les détails pour rendre votre voyage mémorable.
            </p>
          </div>
          <div ref={stepThreeRef} className="process-bloc step-3">
            <div className="badge">3</div>
            <h3>Photo de profil</h3>
            <p>
              Alors, n'attendez plus et téléchargez votre photo de profil dès
              maintenant pour commencer à explorer le monde virtuel du voyage
              avec style !
            </p>
          </div>
          <div ref={stepFourRef} className="process-bloc step-4">
            <div className="badge">4</div>
            <h3>Pièce d'identité</h3>
            <p>
              Scannez votre pièce d'identité pour vérifier votre profil en toute
              sécurité.Protégez votre expérience en vérifiant votre profil dès
              maintenant et profitez pleinement de toutes les fonctionnalités de
              notre plateforme.
            </p>
          </div>
          <div ref={stepFiveRef} className="process-bloc step-5">
            <div className="badge">5</div>
            <h3>Décharge à signer</h3>
            <p>
              Pour compléter votre profil et obtenir le badge de profil vérifié,
              veuillez signer la décharge et lire attentivement les conditions
              générales. En signant la décharge, vous acceptez de respecter les
              règles et les directives de notre communauté.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};
